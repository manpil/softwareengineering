﻿using System;
using GalaSoft.MvvmLight;
using SoftwareEngineering.Services;

namespace SoftwareEngineering.Models
{
    public class Person : ObservableObject
    {
        private readonly IAgeFormatterService _ageFormatter;

        public Person(IAgeFormatterService ageFormatter)
        {
            _ageFormatter = ageFormatter;
        }

        public string Name { get; set; }

        public DateTime Birthday { get; set; }

        public string Age
        {
            get { return _ageFormatter.Format(Birthday); }
        }
    }
}
