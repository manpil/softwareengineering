﻿using System;

namespace SoftwareEngineering.Services
{
    public interface IAgeFormatterService
    {
        string Format(DateTime birthday);
    }
}
