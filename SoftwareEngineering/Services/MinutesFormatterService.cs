﻿using System;

namespace SoftwareEngineering.Services
{
    public class MinutesFormatterService : IAgeFormatterService
    {
        public string Format(DateTime birthday)
        {
            DateTime now = DateTime.Now;
            TimeSpan elapsed = now.Subtract(birthday);
            double daysAgo = elapsed.TotalDays;
            double minutesAgo = daysAgo*1440;
            return string.Format("{0} Minutes", minutesAgo);
        }
    }
}
