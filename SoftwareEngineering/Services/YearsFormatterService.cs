﻿using System;

namespace SoftwareEngineering.Services
{
    public class YearsFormatterService : IAgeFormatterService
    {
        public string Format(DateTime birthday)
        {
            DateTime now = DateTime.Today;
            int age = now.Year - birthday.Year;
            return string.Format("{0} Years", age);
        }
    }
}
