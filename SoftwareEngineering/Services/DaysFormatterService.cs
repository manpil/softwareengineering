﻿using System;

namespace SoftwareEngineering.Services
{
    class DaysFormatterService : IAgeFormatterService
    {
        public string Format(DateTime birthday)
        {
            DateTime now = DateTime.Now;
            TimeSpan elapsed = now.Subtract(birthday);
            double daysAgo = elapsed.TotalDays;
            return string.Format("{0} Days", daysAgo);
        }
    }
}
