using System;
using System.Collections.ObjectModel;
using GalaSoft.MvvmLight;
using SoftwareEngineering.Models;
using SoftwareEngineering.Services;

namespace SoftwareEngineering.ViewModel
{
    /// <summary>
    /// This class contains properties that the main View can data bind to.
    /// <para>
    /// Use the <strong>mvvminpc</strong> snippet to add bindable properties to this ViewModel.
    /// </para>
    /// <para>
    /// You can also use Blend to data bind with the tool's support.
    /// </para>
    /// <para>
    /// See http://www.galasoft.ch/mvvm
    /// </para>
    /// </summary>
    public class MainViewModel : ViewModelBase
    {
        private readonly IAgeFormatterService _ageFormatter;

        public Person Person { get; set; }

        public ObservableCollection<Person> Persons { get; private set; }

        public Person SelectedPerson { get; set; }

        public IAgeFormatterService AgeFormatter
        {
            get { return _ageFormatter; }
        }

        /// <summary>
        /// Initializes a new instance of the MainViewModel class.
        /// </summary>
        public MainViewModel(IAgeFormatterService ageFormatter)
        {
            _ageFormatter = ageFormatter;
            Person = new Person(ageFormatter);

            Persons = new ObservableCollection<Person>()
            {
                new Person(ageFormatter) {Name = "Herbert Prohaska", Birthday = new DateTime(1955, 8, 8)},
                new Person(ageFormatter) {Name = "Hans Krankl", Birthday = new DateTime(1953, 2, 14)},
                new Person(ageFormatter) {Name = "David Alaba", Birthday = new DateTime(1992, 6, 24)}
            };
        }
    }
}